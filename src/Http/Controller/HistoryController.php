<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HistoryController
{
    public function index(Request $request)
    {
        // todo: modify codes to get history
        dd('create history logic here');
    }

    public function show($id)
    {
        dd('create show history by id here');
    }

    public function remove($id)
    {
        // todo: modify codes to remove history
        dd('create remove history logic here');
    }
}
